package redgrams.examples;

import org.websint.i.bukkitredgram.IOutputport;
import org.websint.i.bukkitredgram.Igram;
import org.websint.i.bukkitredgram.Iinputport;
import org.websint.i.bukkitredgram.ult.bitop;

public class numbershower implements Igram {

	IOutputport a,b,c,d,e,f,g;
	protected Iinputport[] ipts;
	protected byte numbernow=0;
	public numbershower(Iinputport[] ipt, IOutputport[] iot) {
		if(ipt.length!=4){
			throw new RuntimeException("ipt.length must be 4");
		}
		ipts=ipt;
		if(iot.length!=7){
			throw new RuntimeException("iot.lenght must be 7");
		}
		a=iot[0];
		b=iot[1];
		c=iot[2];
		d=iot[3];
		e=iot[4];
		f=iot[5];
		g=iot[6];
	}
	
	@Override
	public void dissetup() {
		a.writeBit(false);
		b.writeBit(false);
		c.writeBit(false);
		d.writeBit(false);
		e.writeBit(false);
		f.writeBit(false);
		g.writeBit(false);
	}

	@Override
	public Iinputport[] getInputs() {
		return null;
	}

	@Override
	public IOutputport[] getOutputs() {
		return null;
	}

	@Override
	public void loop() {
		numbernow=(byte) bitop.rtoi(ipts);
		String dvs;
		switch (numbernow) {
		case 0:
			dvs="abcefg"; break;
		case 1:
			dvs="cf"; break;
		case 2:
			dvs="acdeg"; break;
		case 3:
			dvs="acdfg"; break;
		case 4:
			dvs="bcdf"; break;
		case 5:
			dvs="abdfg"; break;
		case 6:
			dvs="abdefg"; break;
		case 7:
			dvs="acf"; break;
		case 8:
			dvs="abcdefg"; break;
		case 9:
			dvs="abcdfg"; break;
		case 10:
			dvs="abcdef"; break;
		case 11:
			dvs="bdefg"; break;
		case 12:
			dvs="deg"; break;
		case 13:
			dvs="cdefg"; break;
		case 14:
			dvs="abdeg"; break;
		case 15:
			dvs="abde"; break;
		default:
			dvs="";
			break;
		}
		a.writeBit(false);
		b.writeBit(false);
		c.writeBit(false);
		d.writeBit(false);
		e.writeBit(false);
		f.writeBit(false);
		g.writeBit(false);
		for(int i=0;i<dvs.length();i++){
			char d=dvs.charAt(i);
			switch (d) {
			case 'a':
				a.writeBit(true); break;
			case 'b':
				b.writeBit(true); break;
			case 'c':
				c.writeBit(true); break;
			case 'd':
				this.d.writeBit(true); break;
			case 'e':
				e.writeBit(true); break;
			case 'f':
				f.writeBit(true); break;
			case 'g':
				g.writeBit(true); break;
			default:
				break;
			}
		}
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void setup() {
		a.writeBit(false);
		b.writeBit(false);
		c.writeBit(false);
		d.writeBit(false);
		e.writeBit(false);
		f.writeBit(false);
		g.writeBit(false);
	}

}
