package redgrams.examples;

import java.util.Arrays;

import org.websint.i.bukkitredgram.BlockInput;
import org.websint.i.bukkitredgram.IOutputport;
import org.websint.i.bukkitredgram.Igram;
import org.websint.i.bukkitredgram.Iinputport;
import org.websint.i.bukkitredgram.ult.bitop;

public class boom implements Igram {

	protected Iinputport cfb;
	protected Iinputport[] num;
	public  boom(Iinputport[] ipt, IOutputport[] iot) {
		if(ipt.length!=9){
			throw new RuntimeException("ipt.length must be 5");
		}
		num=Arrays.copyOfRange(ipt, 0, 9);
		cfb=ipt[8];
	}
	@Override
	public void dissetup() {
	}

	@Override
	public Iinputport[] getInputs() {
		return null;
	}

	@Override
	public IOutputport[] getOutputs() {
		return null;
	}

	@Override
	public void loop() {
		int boomqd=(int) bitop.rtoi(num);
		boolean ifbom=cfb.readBit();
		if(ifbom){
			((BlockInput)cfb).getBlock().getWorld().createExplosion(((BlockInput)cfb).getBlock().getLocation(), boomqd);
		}
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void setup() {
	}

}
