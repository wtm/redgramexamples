package redgrams.examples;

import org.websint.i.bukkitredgram.IOutputport;
import org.websint.i.bukkitredgram.Igram;
import org.websint.i.bukkitredgram.Iinputport;
import org.websint.i.bukkitredgram.ult.bitop;

public class flowclock implements Igram {

	protected IOutputport[] tsd;
	protected long clock;
	public long getClock() {
		return clock;
	}
	public void setClock(long clock) {
		this.clock = clock;
	}
	public flowclock(Iinputport[] ipt, IOutputport[] iot) {
		if(iot.length<1){
			throw new RuntimeException("need at last one output");
		}
		if(ipt.length>0){
			throw new RuntimeException("this stuff don't need input :)");
		}
		tsd=iot;
	}
	@Override
	public void dissetup() {
		for (IOutputport iOutputport : tsd) {
			iOutputport.writeBit(false);
		}
	}

	@Override
	public Iinputport[] getInputs() {
		return null;
	}

	@Override
	public IOutputport[] getOutputs() {
		return null;
	}
	protected int status=0;

	@Override
	public void loop() {
		if(status!=0){
			status=0;
			return;
		}
		clock++;
		bitop.itor(clock, tsd);
		status=1;
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void setup() {
		clock=0;
	}

}
