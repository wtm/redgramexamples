package redgrams.examples;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.websint.i.bukkitredgram.BlockInput;
import org.websint.i.bukkitredgram.BlockOutput;
import org.websint.i.bukkitredgram.IOutputport;
import org.websint.i.bukkitredgram.Igram;
import org.websint.i.bukkitredgram.Iinputport;
import org.websint.i.bukkitredgram.ult.bitop;

public class canvas implements Igram {

	protected Iinputport[] xinp,yinp;
	protected Iinputport readsn, writesn, set;
	protected IOutputport out;
	protected IOutputport[][] ad=new IOutputport[194][109];
	protected Location sa;
	public canvas(Block b) {
		Location ta=b.getLocation();
		Location sf=new Location(ta.getWorld(), ta.getX()+3, ta.getY(), ta.getZ());
		xinp=new Iinputport[10];
		yinp=new Iinputport[10];
		for(int i=0;i<10;i++){
			xinp[i]=new BlockInput(sf.getBlock());
			sf.add(2, 0, 0);
		}
		for(int i=0;i<10;i++){
			yinp[i]=new BlockInput(sf.getBlock());
			sf.add(2, 0, 0);
		}
		sf.add(2, 0, 0);
		readsn=new BlockInput(sf.getBlock());
		sf.add(2, 0, 0);
		writesn=new BlockInput(sf.getBlock());
		sf.add(4, 0, 0);
		set=new BlockInput(sf.getBlock());
		sf.add(2, 0, 0);
		out=new BlockOutput(sf.getBlock());
		out.writeBit(false);
		sa=new Location(ta.getWorld(), ta.getX()+6, ta.getY()-1, ta.getZ()-114);
	}
	protected void setpx(int x, int z, boolean ds){
		try{
			IOutputport xzs=ad[x][z];
			if(xzs==null){
				xzs=new BlockOutput(sa.clone().add(x, 0, z).getBlock());
				ad[x][z]=xzs;
			}
			ad[x][z].writeBit(ds);
		}catch(ArrayIndexOutOfBoundsException e){
			return;
		}
	}
	@Override
	public void dissetup() {
		for (Iinputport ipt : xinp) {
			setWool(ipt);
		}
		for (Iinputport ipt : yinp) {
			setWool(ipt);
		}
		setWool(readsn);
		setWool(writesn);
		setWool(set);
		for (IOutputport[] ios : ad) {
			for (IOutputport io : ios) {
				if(io!=null){
					io.writeBit(false);
				}
			}
		}
	}
	
	@SuppressWarnings("deprecation")
	protected void setWool(Iinputport ipt){
		Block ipts=((BlockInput)ipt).getBlock();
		ipts.setType(Material.WOOL);
		ipts.setData((byte)15);
	}

	@Override
	public Iinputport[] getInputs() {
		return null;
	}

	@Override
	public IOutputport[] getOutputs() {
		return null;
	}
	
	@Override
	public void loop() {
		int xip=(int) bitop.rtoi(xinp);
		int yip=(int) bitop.rtoi(yinp);
		if(writesn.readBit()){
			setpx(xip, yip, set.readBit());
		}else if(readsn.readBit()){
			IOutputport xzs=ad[xip][yip];
			out.writeBit(xzs!=null && ((BlockOutput)xzs).getBlock().isBlockPowered());
		}
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void setup() {
	}

}
